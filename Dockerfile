FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["Made.CitationNetwork.Api/Made.CitationNetwork.WebApi/Made.CitationNetwork.WebApi.csproj", "Made.CitationNetwork.WebApi/"]

RUN dotnet restore "Made.CitationNetwork.WebApi/Made.CitationNetwork.WebApi.csproj"
COPY ./Made.CitationNetwork.Api ./
WORKDIR "/src/Made.CitationNetwork.WebApi"
RUN dotnet build "Made.CitationNetwork.WebApi.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Made.CitationNetwork.WebApi.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Made.CitationNetwork.WebApi.dll"]
