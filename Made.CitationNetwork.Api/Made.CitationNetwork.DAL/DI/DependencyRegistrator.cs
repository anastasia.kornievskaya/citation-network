﻿using Made.CitationNetwork.DAL.Interfaces;
using Made.CitationNetwork.DAL.Models;
using Made.CitationNetwork.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Made.CitationNetwork.DAL.DI
{
    public static class DependencyRegistrator
    {
        private const string connectionStringName = "DefaultConnection";

        public static IServiceCollection AddDataAccessDatabaseComponents(this IServiceCollection services, IConfiguration configuration)
        {
            AddDbContext(services, configuration);

            AddRepositories(services);

            AddUnitOfWork(services);

            return services;
        }

        private static void AddDbContext(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CitationNetworkContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString(connectionStringName)));
        }

        private static void AddRepositories(IServiceCollection services)
        {
            services.AddTransient<IRepository<Articles>, Repository<Articles>>();
            services.AddTransient<IRepository<Authors>, Repository<Authors>>();
        }

        private static void AddUnitOfWork(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }

}
