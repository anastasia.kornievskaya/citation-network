﻿using Made.CitationNetwork.DAL.Interfaces;
using Made.CitationNetwork.DAL.Models;
using Made.CitationNetwork.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Made.CitationNetwork.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private IRepository<Articles> _articlesRepository;

        private IRepository<Authors> _authorsRepository;

        private readonly CitationNetworkContext _context;


        public IRepository<Articles> Articles => GetRepository(ref _articlesRepository);

        public IRepository<Authors> Authors => GetRepository(ref _authorsRepository);

        public UnitOfWork(CitationNetworkContext context)
        {
            _context = context;
        }

        public async Task<bool> SaveChangesAsync()
        {
            int changes = await _context.SaveChangesAsync();

            return changes > 0;
        }

        private IRepository<T> GetRepository<T>(ref IRepository<T> repositoryField) where T : class
        {
            return repositoryField ??= new Repository<T>(_context);
        }
    }

}
