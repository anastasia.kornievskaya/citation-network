﻿using Made.CitationNetwork.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Made.CitationNetwork.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Articles> Articles { get; }

        IRepository<Authors> Authors { get; }

        Task<bool> SaveChangesAsync();
    }

}
