﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Made.CitationNetwork.DAL.Models
{
    public class Authors
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public ICollection<ArticlesAuthors> ArticlesAuthors { get; set; }
        public virtual ICollection<Orgs> Orgs { get; set; }
    }
}
