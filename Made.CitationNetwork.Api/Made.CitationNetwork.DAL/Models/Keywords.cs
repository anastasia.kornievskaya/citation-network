﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Made.CitationNetwork.DAL.Models
{
    public class Keywords
    {
        public int Id { get; set; }
        public string Sentence { get; set; }
        public string ArticleId { get; set; }

        public virtual Articles Article { get; set; }
    }
}
