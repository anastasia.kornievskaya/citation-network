﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Made.CitationNetwork.DAL.Models
{
    public class Articles
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public short? Year { get; set; }
        public int? GlobalCitationNumber { get; set; }
        public int? LocalCitationNumber { get; set; }

        public ICollection<ArticlesAuthors> ArticlesAuthors { get; set; }
        public ICollection<ArticlesVenues> ArticlesVenues { get; set; }
        public virtual ICollection<Abstract> Abstract { get; set; }
        public virtual ICollection<Fos> Fos { get; set; }
        public virtual ICollection<Keywords> Keywords { get; set; }
        public virtual ICollection<References> References { get; set; }
    }
}
