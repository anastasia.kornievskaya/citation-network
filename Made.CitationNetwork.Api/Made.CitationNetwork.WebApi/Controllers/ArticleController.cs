﻿using Made.CitationNetwork.BLL.Services.Interfaces;
using Made.CitationNetwork.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Made.CitationNetwork.WebApi.Controllers
{
    [Route("api/articles")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticlesService articlesService;

        public ArticlesController(IArticlesService articlesService)
        {
            this.articlesService = articlesService ?? throw new ArgumentNullException(nameof(articlesService));
        }

        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAllArticles()
        {
            var articles = await articlesService.GetAllArticlesAsync();

            return Ok(articles);
        }

    }
}
