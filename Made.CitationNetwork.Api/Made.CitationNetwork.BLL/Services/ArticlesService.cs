﻿using Made.CitationNetwork.BLL.Services.Interfaces;
using Made.CitationNetwork.DAL.Interfaces;
using Made.CitationNetwork.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Made.CitationNetwork.BLL.Services
{
    public class ArticlesService : BaseService, IArticlesService
    {
        public ArticlesService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<IEnumerable<Articles>> GetAllArticlesAsync()
        {
            var articles = await UnitOfWork.Articles
                .GetAll()
                .Include(a => a.ArticlesAuthors).ThenInclude(aa => aa.AuthorNavigation)
                .Include(a => a.ArticlesVenues).ThenInclude(av => av.VenueNavigation)
                .Include(a => a.Abstract)
                .Include(a => a.Fos)
                .Include(a => a.Keywords)
                .Include(a => a.References)
                .ToListAsync();

            return articles;
        }
    }
}
