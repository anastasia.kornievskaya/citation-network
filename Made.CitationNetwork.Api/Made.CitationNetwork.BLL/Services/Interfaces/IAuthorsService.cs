﻿using Made.CitationNetwork.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Made.CitationNetwork.BLL.Services.Interfaces
{
    public interface IAuthorsService
    {
        Task<IEnumerable<Authors>> GetAllAuthorsAsync();
    }
}
