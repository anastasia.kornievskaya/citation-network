﻿using Made.CitationNetwork.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Made.CitationNetwork.BLL.Services
{
    public abstract class BaseService
    {
        protected IUnitOfWork UnitOfWork { get; }

        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }
    }
}
